"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCardItems = [];
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    //gọi hàm load trang
    onPageLoading();

    //Tạo mảng card để lưu sản phẩm khi chọn sản phẩm
   

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //Hàm load  dữ liệu 
    function onPageLoading() {
        //Gọi hàm lấy danh sách piza
        CallApiGetListPizzaObj();
        //Gọi hàm lấy danh sách blog
        CallApiGetListBlogObj();
        //Gọi hàm visited để lấy giỏ hàng hoặc thiết lập giỏ hàng
        visited();
    }
    //Hàm khi nhấn nút btn-add-card-product
    $("#list-pizza-product").on("click", ".btn-add-card-product", function() {
        onBtnAddCardProductClick(this);
    })
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm call Api để lấy dánh sách pizza
    function CallApiGetListPizzaObj() {
        $.ajax({
            url: "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/" + "/pizza",
            type: "GET",
            success: function (paramListPizzaObj) {
                console.log(paramListPizzaObj)
                CreateListPizzaToHtml(paramListPizzaObj)
            },
            error: function (ajaxContext) {
                console.log(ajaxContext.responseText)
            }
        })
    }
    //Hàm tạo danh sách pizza ra HTML
    function CreateListPizzaToHtml(paramListPizzaObj) {
        //Lấy thẻ chứa danh sách pizza
        var vDivListPizzas = $("#list-pizza-product");
        //Xóa dữ liệu sẵn có của list pizza
        vDivListPizzas.html("");
        //Duyệt từng sản phẩm pizza add vào danh sách
        for (let bI = 0; bI < paramListPizzaObj.length; bI++) {
            //Tạo col chứa pizza
            var vDivCol = $("<div>", { class: "col-md-6 col-lg-3 card-box" }).appendTo(vDivListPizzas);

            // tạo từng pizza product
            var vPizzaProduct = CreatePizzaProduct(paramListPizzaObj[bI]);
            //add pizza product vào col
            vPizzaProduct.appendTo(vDivCol);
        }
    }
    //Hàm tạo pizza product
    function CreatePizzaProduct(paramPizzaProductObj) {
        //Tạo card chưa
        var vDivCard = $("<div>", { class: "card product-card" });
        //1. tạo div chứa picture và sale off    
        var vDivCardPicture = $("<div>", { class: "card-picture product-card-picture" }).appendTo(vDivCard);
        //Tạo img product
        $("<img>", { class: "card-img-top", src: paramPizzaProductObj.imageUrl, alt: "Card image cap" }).appendTo(vDivCardPicture);
        //Tạo span giảm giá
        if (paramPizzaProductObj.rating <= 4.5) {
            $("<span>", { class: "Product-sales-off", html: "20%" }).appendTo(vDivCardPicture);
        }
        //2. Tạo card body 
        var vDivCardBody = $("<div>", { class: "card-body" }).appendTo(vDivCard);
        //Tạo div Product-introduction
        var vDivIntroduction = $("<div>", { class: "product-introduction" }).appendTo(vDivCardBody);
        //tạo thẻ chứa loại pizza
        $("<p>", { class: "product-title", html: paramPizzaProductObj.name }).appendTo(vDivIntroduction);
        //Tạo thẻ giá    
        $("<p>", { class: "product-price", html: paramPizzaProductObj.price }).appendTo(vDivIntroduction);
        //Tạo div Card-rating append to card-body
        var vDivCardRating = $("<div>", { class: "card-rating" }).appendTo(vDivCardBody);
        //Tạo card-vote append to card-rating
        var vDivCardVote = $("<div>", { class: "card-vote" }).appendTo(vDivCardRating);
        //tạo card -star append to card-vote
        var vDivCardstar = $("<div>", { class: "card-star" }).appendTo(vDivCardVote);
        //Tạo Img trong card-star
        $("<img>", { class: "card-star-img", src: "./images/star-vote.png" }).appendTo(vDivCardstar);
        //Tạo thẻ span số lương sao trong card-star
        $("<span>", { class: "card-star-num", html: paramPizzaProductObj.rating }).appendTo(vDivCardstar);
        //tạo card-time append to card-vote
        $("<span>", { class: "card-star-time", html: paramPizzaProductObj.time }).appendTo(vDivCardVote);
        //Tạo button append  vDivCardRating
        var vButton = $("<button>", { class: "btn-add-card-product" })
            .data({"id" : paramPizzaProductObj.id, "imgsrc": paramPizzaProductObj.imageUrl,
                  "price" : paramPizzaProductObj.price, "name": paramPizzaProductObj.name, 
                  "time" : paramPizzaProductObj.time })
            .appendTo(vDivCardRating);
        //Tạo img card-kitchen-plus-img append to card-rating
        $("<img>", { class: "card-kitchen-plus-img", src: "./images/kitchen_plus.png" }).appendTo(vButton);
        return vDivCard;
    }

    // //Hàm tạo xem có tạo card trên local storgage chưa
    // function visited() {
    //     var getCardFromStore = localStorage.getItem("cartItems");
    //     if(getCardFromStore != null && getCardFromStore) {
    //         gCardItems = JSON.parse(getCardFromStore);
    //     }
    //     else{
    //         var vstringify = JSON.stringify(gCardItems);
    //         localStorage.setItem("cartItems", vstringify);
    //     }
    //     checkCardItems(gCardItems);
        
    // }

    //Hàm khi click vào btn-add-product
    // function onBtnAddCardProductClick(paramButton) {
    //      //Lấy Id của product
    //      var vId = $(paramButton).data("id");
    //      var vName = $(paramButton).data("name");
    //      var vSrc = $(paramButton).data("imgsrc");
    //      var vPrice = $(paramButton).data("price");
    //      var vTime = $(paramButton).data("time");
    //      console.log(vSrc)

    //      //Tim xem Id có trong mảng chứa item hay không
    //      var vIndex = gCardItems.findIndex(x=>x.id == vId);
    //      console.log(vIndex);

    //      if(vIndex == - 1) {
    //         //nếu không tìm thấy
    //         gCardItems.push({id : vId, sl: 1, name: vName, src: vSrc, price: vPrice, time: vTime});
    //         console.log(gCardItems);
    //      }
    //      else{
    //         //nếu đã tồn tại trong giỏ hàng, thêm số lương 1
    //         gCardItems[vIndex].sl ++;
    //      }
         
    //      //lưu vào local store
    //      var vstringify = JSON.stringify(gCardItems);
    //      localStorage.setItem("cartItems", vstringify);
    //      //Check lại cart
    //      checkCardItems(gCardItems);
    // }
    // //Hàm kiểm tra giỏ hàng còn có sản phẩm hay không
    // function checkCardItems(paramCardItem) {
    //     //thay đổi màu cart
    //     if(paramCardItem.length > 0) {
    //         $("#items-cart-link").addClass("cart-get-items")
    //     }
    //     else{
    //         $("#items-cart-link").removeClass("cart-get-items")
    //     }
    // }
   
})